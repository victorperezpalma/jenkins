package personal.javajenkins;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import personal.javajenkins.service.impl.TodoService;

@SpringBootTest
@AutoConfigureMockMvc
class TodoController {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TodoService todoService;

	@Test
	void testRest() throws Exception {
		mockMvc.perform(get("/home/todo/")).andDo(print()).andExpect(status().isOk());

	}

	@Test
	void testService() throws Exception {
		int l = todoService.findTodo().size();
		assert (l == 0);
	}

}
