package personal.javajenkins;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;

import personal.javajenkins.dao.TodoDao;
import personal.javajenkins.entity.Todo;

@SpringBootTest
@DataJpaTest
class TodoDaoTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private TodoDao todoDao;

	@Test
	void testDao() throws Exception {
		long c = todoDao.count();
		Todo todo = Todo.builder().todo("todo0").build();
		todo = todoDao.save(todo);
		long c1 = todoDao.count();
		assert(c+1==c1);
	}

}
