package personal.javajenkins.mapper;

import personal.javajenkins.dto.TodoDto;
import personal.javajenkins.entity.Todo;

public class TodoMapper {

	public static TodoDto mapper(Todo todo) {
		TodoDto todoDto = TodoDto.builder().id(todo.getId()).todo(todo.getTodo()).build();
		return todoDto;
	}
}
