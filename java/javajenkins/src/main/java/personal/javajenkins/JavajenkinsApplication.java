package personal.javajenkins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavajenkinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavajenkinsApplication.class, args);
	}

}
