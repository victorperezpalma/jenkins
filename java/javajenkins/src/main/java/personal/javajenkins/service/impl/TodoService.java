package personal.javajenkins.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import personal.javajenkins.dao.TodoDao;
import personal.javajenkins.dto.TodoDto;
import personal.javajenkins.entity.Todo;
import personal.javajenkins.mapper.TodoMapper;
import personal.javajenkins.service.ITodoService;

@Service
@Slf4j
public class TodoService implements ITodoService {

	@Autowired
	private TodoDao todoDao;

	@Override
	@Transactional(readOnly = true)
	public List<TodoDto> findTodo() {
		List<TodoDto> todoList = todoDao.findAll().stream().map(t -> TodoMapper.mapper(t)).collect(Collectors.toList());
		return todoList;
	}

	@Override
	@Transactional
	public TodoDto createTodo(TodoDto todoDto) {
		Todo todo = Todo.builder().todo(todoDto.getTodo()).build();
		todoDao.save(todo);
		return TodoMapper.mapper(todo);
	}

}
