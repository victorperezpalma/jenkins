package personal.javajenkins.service;

import java.util.List;

import personal.javajenkins.dto.TodoDto;

public interface ITodoService {

	public List<TodoDto> findTodo();

	public TodoDto createTodo(TodoDto todoDto);

}
