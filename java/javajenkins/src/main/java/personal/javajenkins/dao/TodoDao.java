package personal.javajenkins.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import personal.javajenkins.entity.Todo;

@Repository
public interface TodoDao extends JpaRepository<Todo, Long> {

}
