package personal.javajenkins.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import personal.javajenkins.dto.TodoDto;
import personal.javajenkins.service.impl.TodoService;

@RestController
@CrossOrigin
@RequestMapping(path = "/home")
public class HomeController {

	@Autowired
	private TodoService todoService;

	@GetMapping("/")
	public ResponseEntity<Map<String, Object>> home() {
		Map<String, Object> mapResults = new HashMap<>();
		mapResults.put("view", "home");
		return ResponseEntity.ok(mapResults);
	}

	@GetMapping("/todo")
	public ResponseEntity<Map<String, Object>> todo() {
		Map<String, Object> mapResults = new HashMap<>();
		List<TodoDto> todoList = todoService.findTodo();
		mapResults.put("todo", todoList);
		return ResponseEntity.ok(mapResults);
	}

}
